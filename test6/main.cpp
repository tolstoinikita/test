#include <iostream>

struct A
{
    A(){std::cout<<"A"<<std::endl;}
};
struct B
{
    B(){std::cout<<"B"<<std::endl;}
};
struct C
{
    C(){std::cout<<"C"<<std::endl;}
};
struct D
{
    D(){std::cout<<"D"<<std::endl;}
};
struct E
{
    E(){std::cout<<"E"<<std::endl;}
};
struct F
{
    F(){std::cout<<"F"<<std::endl;}
};

class Probe:public C, virtual public D,public A, virtual public B
{
public:
    Probe():f(),A(),B()
    {
        std::cout<<"Probe"<<std::endl;
    }
private:
    E e;
    F f;
};
int main()
{
    Probe foo;
    return 0;
}
