#include <iostream>

class A
{public:
    int X1;
protected:
    int X2;
private:
    int X3;
};

class B:public A
{public:
    B()
    {
     X1 = 10;
     X2 = 20;
     X3 = 30;
    }

};


int main()
{
    B b;
    return 0;
}
