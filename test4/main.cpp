#include <iostream>

using namespace std;

class A
{
public:
    A(int i):v(i)
    {
        if (i !=0) throw runtime_error("failed");
        cout<<"A()"<<endl;
    }
    ~A(){
    cout<<"~A()" <<endl;
    }
private:
    int v;
};

class B
{
public:
    B():a1(0),a2(1)
    {
        cout<<"B()"<<endl;
    }
    ~B()
    {
    cout<<"~B()"<<endl;
    }
private:
    A a1;
    A a2;
};
int main()
{
    try
    {
        B b;
    }catch(...){}
    return 0;
}
