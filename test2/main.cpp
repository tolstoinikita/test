#include <iostream>

class Base
{
public:
    virtual const char* name()=0;
};
class Derrived:public Base
{
public:
    virtual const char* name()
     {return "D";}
};
int main()
{
    Derrived d;
    Base& b=d;
    std::cout<<b.name()<<std::endl;

    return 0;
}
