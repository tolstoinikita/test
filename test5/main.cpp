#include <iostream>
#include <algorithm>
#include <list>

//using namespace std;

bool Predicate(int item)
{
return item % 2==0;
}

void DeleteAndPrintItems()
{
std::list<int>items={0,1,2,3,4,5};
std::remove_if(items.begin(),items.end(),Predicate);
std::cout<<items.front()<<items.size()<<std::endl;
}
int main()
{
   DeleteAndPrintItems();
    return 0;
}
