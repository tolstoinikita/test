#include <iostream>
#include <map>
using namespace std;

class Coordinates
{
public:
    Coordinates(int x,int y):X(x),Y(y){}

bool operator < (const Coordinates& coord) const
{
    return X < coord.X && Y < coord.Y;
}
private:
    int X;
    int Y;
};
void PrintCoordinates()
{
map<Coordinates,int>location;
const Coordinates coord1(0,1);
const Coordinates coord2(1,0);
location[coord1]=36;
location[coord2]=78;
cout<<location[coord1]<<location[coord2]<<endl;
}
int main()
{
    PrintCoordinates();
    return 0;
}
